class Contact{
    constructor(name, numberPhone, mail){
        this.name = name;
        this.numberPhone = numberPhone;
        this.mail = mail;
    }
}

class ListContact{
    constructor(){
        this.listAddress = [];
    }

    addNewContact(contact){
        this.listAddress.push(contact);
    }

    showContact(){
        this.listAddress.forEach(contact => 
            console.log(`Nom : ${contact.name}\nNuméro : ${contact.numberPhone}\nMail : ${contact.mail}`)    
        )
    }

    searchContact(name){
        return this.listAddress.find(contact => contact.name.toLowerCase() === name);
    }

    deleteContact(name){
        this.listAddress = this.listAddress.filter(contact => contact.name.toLowerCase() !== name )
    }
}

const carnet = new ListContact();

//Création de nouveaux contacts
const caroline = new Contact("Caroline", +33784923549, "caro.line@yahoo.com");
const rosa = new Contact("Rosa", +33654370999, "rosa.lg@gmail.com");
const adixia = new Contact("Adixia", +33534370999, "adi.dj@outlook.fr");

//AJouter au carnet d'adresse les contacts suivants
carnet.addNewContact(caroline);
carnet.addNewContact(rosa);
carnet.addNewContact(adixia);

//Afficher le carnet d'adresse
carnet.showContact();

//Rechercher un contact par nom
const filteredContact = carnet.searchContact("rosa");
console.log(filteredContact);

//Supprimer un contact en fontion du nom
carnet.deleteContact("caroline");
carnet.showContact();