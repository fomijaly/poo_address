# Mini-projet : Carnet d'Adresses

## Description :
Créez un programme de gestion de contacts en utilisant la programmation orientée objet. Chaque contact devrait avoir un nom, un numéro de téléphone et une adresse e-mail.

## Fonctionnalités attendues :

- Ajouter un nouveau contact avec un nom, un numéro de téléphone et une adresse e-mail. ✅
- Afficher la liste des contacts. ✅
- Rechercher un contact par nom. ✅
- Supprimer un contact. ✅

### Contraintes techniques :
Utilisez au moins une classe pour représenter un contact et une autre pour gérer le carnet d'adresses.
Utilisez les concepts d'encapsulation et de méthodes pour manipuler les contacts.

## Bonus (optionnel) :
- Ajoutez une fonction pour modifier les informations d'un contact existant.
- Implémentez une interface utilisateur simple en ligne de commande.